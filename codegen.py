from __future__ import nested_scopes
from __future__ import division

from amqp_codegen import *
import string
import re

types = {
		'octet': 'ShortShortUint',
		'short': 'ShortUint',
		'long': 'LongUint',
		'longlong': 'LongLongUint',
		'shortstr': 'ShortString',
		'longstr': 'LongString',
		'bit': 'u8',
		'table': 'Table',
		'timestamp': 'Timestamp',
}

def typeFor(spec, f):
	return types[spec.resolveDomain(f.domain)]

def camelCase(name):
	parts = re.split("[- ]", name)
	rustStructName = ""
	for part in parts:
		rustStructName = rustStructName + part[0].upper() + part[1:].lower()
	return rustStructName

def constantCase(name):
	return re.sub("[- ]", "_", name.upper())

def underscoreCase(name):
	return re.sub("[- ]", "_", name.lower())

def genMethods(spec):
	def parameterList(fields):
		if fields:
			return '(%s)' % (', '.join([typeFor(spec, f) for f in fields]))
		else:
			return ''

	def parameterName(field):
		return "f_" + underscoreCase(field.name)

	print """// Generated code. Do not edit. Edit and re-run codegen.py instead.

use std::io;

use transport::types::*;
use transport::serialization::{Readable,Writable};
use transport::framing::{Frame,MethodFrame,Channel};

#[deriving(Show)]
pub enum Method {"""

	for c in spec.allClasses():
		for m in c.methods:
			print "  %s%s%s," % (
				camelCase(c.name),
				camelCase(m.name),
				parameterList(m.arguments)
			)

	print """}

impl Method {
    pub fn from_frame(frame: Frame) -> Option<Method> {
        match frame {
            MethodFrame(_, payload) => Some(Method::new(payload)),
            _ => None
        }
    }

    pub fn new(payload: Vec<u8>) -> Method {
        let mut reader = io::MemReader::new(payload);
        let class_id = reader.read_be_u16().unwrap();
        let method_id = reader.read_be_u16().unwrap();

        match (class_id, method_id) {"""

	for c in spec.allClasses():
		for m in c.methods:
			args = ""
			if m.arguments:
				args = "(%s)" % (", ".join(["Readable::from_reader(&mut reader).unwrap()" for _ in m.arguments]))

			print "            (%d, %d) => %s%s%s," % (c.index, m.index, camelCase(c.name), camelCase(m.name), args)

	print """            _ => unimplemented!(),
       }
    }

    pub fn payload(&self) -> Vec<u8> {
        let mut writer = io::MemWriter::new();

        match *self {"""


	for c in spec.allClasses():
		for m in c.methods:
			args = ""
			if m.arguments:
				args = "(%s)" % (", ".join(["ref " + parameterName(f) for f in m.arguments]))

			print "            %s%s%s => {" % (camelCase(c.name), camelCase(m.name), args)
			print "                writer.write_be_u16(%d).ok().expect(\"something went horribly wrong\");" % (c.index)
			print "                writer.write_be_u16(%d).ok().expect(\"something went horribly wrong\");" % (m.index)
			print

			for f in m.arguments:
					print "                %s.to_writer(&mut writer).ok().expect(\"something went horribly wrong\");" % (parameterName(f))

			print "            },"

	print """        }

        writer.unwrap()
    }

    pub fn to_frame(&self, channel: Channel) -> Frame {
        MethodFrame(channel, self.payload())
    }
}"""


def generateMethods(specPath):
	genMethods(AmqpSpec(specPath))

if __name__ == "__main__":
	do_main_dict({"methods":generateMethods})
