extern crate amqp;

use amqp::connection;
use std::io::timer::sleep;
use std::io::IoResult;

fn run_test() -> IoResult<()> {
	let conn = try!(connection::Connection::new("amqp://localhost/"));
	let channel = try!(conn.create_channel());
	let queue = try!(channel.open_queue("examples.hello_world"));
	let exchange = try!(channel.default_exchange());

	queue.subscribe(|_, _, payload| {
		println!("Got something!: {}", payload);
	});

	try!(exchange.publish("Hello!".as_bytes(), queue.name()));

	sleep(1000);

	try!(conn.close());

	Ok(())
}

fn main() {
	match run_test() {
		Err(e) => fail!("{}", e),
		_ => println!("done!")
	}
}
