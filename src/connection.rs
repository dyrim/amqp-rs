use std::io;
use std::cmp;
use std::num::Zero;
use url::Url;

use channel::Channel;
use transport::framing::FrameBuffer;
use transport::framing;
use transport::methods;
use transport::types::{Table,LongString,ShortStringCast};
use transport::serialization::Writable;
use transport::heartbeat::HeartbeatSender;

pub struct Connection {
    frame_buffer: FrameBuffer<io::TcpStream>,
    stream: io::BufferedStream<io::TcpStream>,
    server_properties: Option<Table>,
    url: Url,
    heartbeat_sender: HeartbeatSender<io::TcpStream>,
}

pub enum Error {
    Io(io::IoError),
}

pub type ConnectionResult<T> = Result<T, Error>;

impl Connection {
    pub fn new(rawurl: &str) -> io::IoResult<Connection> {
        let url = match from_str::<Url>(rawurl) {
            Some(url) => url,
            None => return Err(io::IoError { kind: io::InvalidInput, desc: "invalid url", detail: None })
        };

        let host = url.host.as_slice();
        let port = match url.port {
            Some(port) => port,
            None => 5672
        };

        let stream = io::BufferedStream::new(try!(io::TcpStream::connect(host, port)));
        let heartbeat_sender = HeartbeatSender::new(stream.get_ref().clone());
        let frame_buffer = FrameBuffer::new(stream.get_ref().clone());

        let mut connection = Connection {
            frame_buffer: frame_buffer,
            stream: stream,
            server_properties: None,
            url: url.clone(),
            heartbeat_sender: heartbeat_sender,
        };

        try!(connection.init_connection());
        try!(connection.open_connection());

        Ok(connection)
    }

    fn send_preamble(&mut self) ->io::IoResult<()> {
        try!(self.stream.write([65, 77, 81, 80, 0, 0, 9, 1]));
        self.stream.flush()
    }

    fn init_connection(&mut self) ->io::IoResult<()> {
        try!(self.send_preamble());

        let connection_start = try!(self.frame_buffer.read_frame());

        match connection_start {
            framing::MethodFrame(0, payload) => match methods::Method::new(payload) {
                methods::ConnectionStart(_, _, server_properties, _, _) => {
                    self.server_properties = Some(server_properties);
                },
                _ => unreachable!()
            },
            _ => unreachable!()
        }

        Ok(())
    }

    fn open_connection(&mut self) ->io::IoResult<()> {
        let mut client_properties = Table::new();
        let mut capabilities = Table::new();
        capabilities.insert("publisher_confirms", true);
        capabilities.insert("consumer_cancel_notify", true);
        capabilities.insert("exchange_exchange_bindings", true);
        capabilities.insert("basic.nack", true);
        capabilities.insert("connection.blocked", true);
        capabilities.insert("authentication_failure_close", true);

        client_properties.insert("capabilities", capabilities);
        client_properties.insert("product", "AMQP.rs");

        let start_ok = methods::ConnectionStartOk(client_properties, "PLAIN".to_short_string(), LongString::from_str("\0guest\0guest"), "en_US".to_short_string());

        try!(start_ok.to_frame(0).to_writer(&mut self.stream));
        try!(self.stream.flush());

        let connection_tune = try!(self.next_non_heartbeat());

        match methods::Method::from_frame(try!(self.next_non_heartbeat())) {
            Some(methods::ConnectionTune(s_channel_max, s_frame_max, heartbeat)) => {
                let frame_max = self.negotiate_value(131072, s_frame_max);
                let channel_max = self.negotiate_value(65535, s_channel_max);

                let tune_ok = methods::ConnectionTuneOk(channel_max, frame_max, heartbeat);
                try!(tune_ok.to_frame(0).to_writer(&mut self.stream));
                try!(self.stream.flush());
            },
            _ => unreachable!(),
        }

        let connection_open = methods::ConnectionOpen(self.url.path.path.to_short_string(), "".to_short_string(), 0);
        try!(connection_open.to_frame(0).to_writer(&mut self.stream));
        try!(self.stream.flush());

        let open_ok = try!(self.next_non_heartbeat());
        match methods::Method::from_frame(try!(self.next_non_heartbeat())) {
            Some(methods::ConnectionOpenOk(_)) => {},
            _ => unreachable!(),
        }

        self.initialize_heartbeat_sender();

        Ok(())
    }

    fn initialize_heartbeat_sender(&mut self) {
        self.heartbeat_sender.start()
    }

    fn negotiate_value<T: Ord + Zero>(&self, client_value: T, server_value: T) -> T {
        if client_value.is_zero() || server_value.is_zero() {
            cmp::max(client_value, server_value)
        } else {
            cmp::min(client_value, server_value)
        }
    }

    fn next_non_heartbeat(&mut self) ->io::IoResult<framing::Frame> {
        let mut frame = try!(self.frame_buffer.read_frame());

        loop {
            match frame {
                framing::HeartbeatFrame => frame = try!(self.frame_buffer.read_frame()),
                _ => return Ok(frame)
            }
        }
    }

    pub fn create_channel(&self) ->io::IoResult<Channel> {
        Ok(Channel)
    }

    pub fn close(mut self) ->io::IoResult<()> {
        let close = methods::ConnectionClose(200, "Goodbye".to_short_string(), 0, 0);
        let frame = framing::MethodFrame(0, close.payload());

        try!(frame.to_writer(&mut self.stream));
        try!(self.stream.flush());

        let mut stream = self.frame_buffer.unwrap();

        try!(stream.close_read());
        try!(stream.close_write());
        Ok(())
    }
}
