use std::io::IoResult;
use exchange::Exchange;
use queue::Queue;

pub struct Channel;

impl Channel {
	pub fn open_queue(&self, queue_name: &str) -> IoResult<Queue> {
		Ok(Queue)
	}

	pub fn default_exchange(&self) -> IoResult<Exchange> {
		Ok(Exchange)
	}
}
