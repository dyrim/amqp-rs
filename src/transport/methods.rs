// Generated code. Do not edit. Edit and re-run codegen.py instead.

use std::io;

use transport::types::*;
use transport::serialization::{Readable,Writable};
use transport::framing::{Frame,MethodFrame,Channel};

#[deriving(Show)]
pub enum Method {
  ConnectionStart(ShortShortUint, ShortShortUint, Table, LongString, LongString),
  ConnectionStartOk(Table, ShortString, LongString, ShortString),
  ConnectionSecure(LongString),
  ConnectionSecureOk(LongString),
  ConnectionTune(ShortUint, LongUint, ShortUint),
  ConnectionTuneOk(ShortUint, LongUint, ShortUint),
  ConnectionOpen(ShortString, ShortString, u8),
  ConnectionOpenOk(ShortString),
  ConnectionClose(ShortUint, ShortString, ShortUint, ShortUint),
  ConnectionCloseOk,
  ConnectionBlocked(ShortString),
  ConnectionUnblocked,
  ChannelOpen(ShortString),
  ChannelOpenOk(LongString),
  ChannelFlow(u8),
  ChannelFlowOk(u8),
  ChannelClose(ShortUint, ShortString, ShortUint, ShortUint),
  ChannelCloseOk,
  AccessRequest(ShortString, u8, u8, u8, u8, u8),
  AccessRequestOk(ShortUint),
  ExchangeDeclare(ShortUint, ShortString, ShortString, u8, u8, u8, u8, u8, Table),
  ExchangeDeclareOk,
  ExchangeDelete(ShortUint, ShortString, u8, u8),
  ExchangeDeleteOk,
  ExchangeBind(ShortUint, ShortString, ShortString, ShortString, u8, Table),
  ExchangeBindOk,
  ExchangeUnbind(ShortUint, ShortString, ShortString, ShortString, u8, Table),
  ExchangeUnbindOk,
  QueueDeclare(ShortUint, ShortString, u8, u8, u8, u8, u8, Table),
  QueueDeclareOk(ShortString, LongUint, LongUint),
  QueueBind(ShortUint, ShortString, ShortString, ShortString, u8, Table),
  QueueBindOk,
  QueuePurge(ShortUint, ShortString, u8),
  QueuePurgeOk(LongUint),
  QueueDelete(ShortUint, ShortString, u8, u8, u8),
  QueueDeleteOk(LongUint),
  QueueUnbind(ShortUint, ShortString, ShortString, ShortString, Table),
  QueueUnbindOk,
  BasicQos(LongUint, ShortUint, u8),
  BasicQosOk,
  BasicConsume(ShortUint, ShortString, ShortString, u8, u8, u8, u8, Table),
  BasicConsumeOk(ShortString),
  BasicCancel(ShortString, u8),
  BasicCancelOk(ShortString),
  BasicPublish(ShortUint, ShortString, ShortString, u8, u8),
  BasicReturn(ShortUint, ShortString, ShortString, ShortString),
  BasicDeliver(ShortString, LongLongUint, u8, ShortString, ShortString),
  BasicGet(ShortUint, ShortString, u8),
  BasicGetOk(LongLongUint, u8, ShortString, ShortString, LongUint),
  BasicGetEmpty(ShortString),
  BasicAck(LongLongUint, u8),
  BasicReject(LongLongUint, u8),
  BasicRecoverAsync(u8),
  BasicRecover(u8),
  BasicRecoverOk,
  BasicNack(LongLongUint, u8, u8),
  TxSelect,
  TxSelectOk,
  TxCommit,
  TxCommitOk,
  TxRollback,
  TxRollbackOk,
  ConfirmSelect(u8),
  ConfirmSelectOk,
}

impl Method {
    pub fn from_frame(frame: Frame) -> Option<Method> {
        match frame {
            MethodFrame(_, payload) => Some(Method::new(payload)),
            _ => None
        }
    }

    pub fn new(payload: Vec<u8>) -> Method {
        let mut reader = io::MemReader::new(payload);
        let class_id = reader.read_be_u16().unwrap();
        let method_id = reader.read_be_u16().unwrap();

        match (class_id, method_id) {
            (10, 10) => ConnectionStart(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (10, 11) => ConnectionStartOk(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (10, 20) => ConnectionSecure(Readable::from_reader(&mut reader).unwrap()),
            (10, 21) => ConnectionSecureOk(Readable::from_reader(&mut reader).unwrap()),
            (10, 30) => ConnectionTune(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (10, 31) => ConnectionTuneOk(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (10, 40) => ConnectionOpen(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (10, 41) => ConnectionOpenOk(Readable::from_reader(&mut reader).unwrap()),
            (10, 50) => ConnectionClose(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (10, 51) => ConnectionCloseOk,
            (10, 60) => ConnectionBlocked(Readable::from_reader(&mut reader).unwrap()),
            (10, 61) => ConnectionUnblocked,
            (20, 10) => ChannelOpen(Readable::from_reader(&mut reader).unwrap()),
            (20, 11) => ChannelOpenOk(Readable::from_reader(&mut reader).unwrap()),
            (20, 20) => ChannelFlow(Readable::from_reader(&mut reader).unwrap()),
            (20, 21) => ChannelFlowOk(Readable::from_reader(&mut reader).unwrap()),
            (20, 40) => ChannelClose(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (20, 41) => ChannelCloseOk,
            (30, 10) => AccessRequest(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (30, 11) => AccessRequestOk(Readable::from_reader(&mut reader).unwrap()),
            (40, 10) => ExchangeDeclare(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (40, 11) => ExchangeDeclareOk,
            (40, 20) => ExchangeDelete(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (40, 21) => ExchangeDeleteOk,
            (40, 30) => ExchangeBind(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (40, 31) => ExchangeBindOk,
            (40, 40) => ExchangeUnbind(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (40, 51) => ExchangeUnbindOk,
            (50, 10) => QueueDeclare(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (50, 11) => QueueDeclareOk(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (50, 20) => QueueBind(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (50, 21) => QueueBindOk,
            (50, 30) => QueuePurge(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (50, 31) => QueuePurgeOk(Readable::from_reader(&mut reader).unwrap()),
            (50, 40) => QueueDelete(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (50, 41) => QueueDeleteOk(Readable::from_reader(&mut reader).unwrap()),
            (50, 50) => QueueUnbind(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (50, 51) => QueueUnbindOk,
            (60, 10) => BasicQos(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 11) => BasicQosOk,
            (60, 20) => BasicConsume(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 21) => BasicConsumeOk(Readable::from_reader(&mut reader).unwrap()),
            (60, 30) => BasicCancel(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 31) => BasicCancelOk(Readable::from_reader(&mut reader).unwrap()),
            (60, 40) => BasicPublish(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 50) => BasicReturn(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 60) => BasicDeliver(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 70) => BasicGet(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 71) => BasicGetOk(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 72) => BasicGetEmpty(Readable::from_reader(&mut reader).unwrap()),
            (60, 80) => BasicAck(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 90) => BasicReject(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (60, 100) => BasicRecoverAsync(Readable::from_reader(&mut reader).unwrap()),
            (60, 110) => BasicRecover(Readable::from_reader(&mut reader).unwrap()),
            (60, 111) => BasicRecoverOk,
            (60, 120) => BasicNack(Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap(), Readable::from_reader(&mut reader).unwrap()),
            (90, 10) => TxSelect,
            (90, 11) => TxSelectOk,
            (90, 20) => TxCommit,
            (90, 21) => TxCommitOk,
            (90, 30) => TxRollback,
            (90, 31) => TxRollbackOk,
            (85, 10) => ConfirmSelect(Readable::from_reader(&mut reader).unwrap()),
            (85, 11) => ConfirmSelectOk,
            _ => unimplemented!(),
       }
    }

    pub fn payload(&self) -> Vec<u8> {
        let mut writer = io::MemWriter::new();

        match *self {
            ConnectionStart(ref f_version_major, ref f_version_minor, ref f_server_properties, ref f_mechanisms, ref f_locales) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(10).ok().expect("something went horribly wrong");

                f_version_major.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_version_minor.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_server_properties.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_mechanisms.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_locales.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionStartOk(ref f_client_properties, ref f_mechanism, ref f_response, ref f_locale) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(11).ok().expect("something went horribly wrong");

                f_client_properties.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_mechanism.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_response.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_locale.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionSecure(ref f_challenge) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(20).ok().expect("something went horribly wrong");

                f_challenge.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionSecureOk(ref f_response) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(21).ok().expect("something went horribly wrong");

                f_response.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionTune(ref f_channel_max, ref f_frame_max, ref f_heartbeat) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(30).ok().expect("something went horribly wrong");

                f_channel_max.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_frame_max.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_heartbeat.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionTuneOk(ref f_channel_max, ref f_frame_max, ref f_heartbeat) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(31).ok().expect("something went horribly wrong");

                f_channel_max.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_frame_max.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_heartbeat.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionOpen(ref f_virtual_host, ref f_capabilities, ref f_insist) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(40).ok().expect("something went horribly wrong");

                f_virtual_host.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_capabilities.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_insist.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionOpenOk(ref f_known_hosts) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(41).ok().expect("something went horribly wrong");

                f_known_hosts.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionClose(ref f_reply_code, ref f_reply_text, ref f_class_id, ref f_method_id) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(50).ok().expect("something went horribly wrong");

                f_reply_code.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_reply_text.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_class_id.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_method_id.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionCloseOk => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(51).ok().expect("something went horribly wrong");

            },
            ConnectionBlocked(ref f_reason) => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(60).ok().expect("something went horribly wrong");

                f_reason.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConnectionUnblocked => {
                writer.write_be_u16(10).ok().expect("something went horribly wrong");
                writer.write_be_u16(61).ok().expect("something went horribly wrong");

            },
            ChannelOpen(ref f_out_of_band) => {
                writer.write_be_u16(20).ok().expect("something went horribly wrong");
                writer.write_be_u16(10).ok().expect("something went horribly wrong");

                f_out_of_band.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ChannelOpenOk(ref f_channel_id) => {
                writer.write_be_u16(20).ok().expect("something went horribly wrong");
                writer.write_be_u16(11).ok().expect("something went horribly wrong");

                f_channel_id.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ChannelFlow(ref f_active) => {
                writer.write_be_u16(20).ok().expect("something went horribly wrong");
                writer.write_be_u16(20).ok().expect("something went horribly wrong");

                f_active.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ChannelFlowOk(ref f_active) => {
                writer.write_be_u16(20).ok().expect("something went horribly wrong");
                writer.write_be_u16(21).ok().expect("something went horribly wrong");

                f_active.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ChannelClose(ref f_reply_code, ref f_reply_text, ref f_class_id, ref f_method_id) => {
                writer.write_be_u16(20).ok().expect("something went horribly wrong");
                writer.write_be_u16(40).ok().expect("something went horribly wrong");

                f_reply_code.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_reply_text.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_class_id.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_method_id.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ChannelCloseOk => {
                writer.write_be_u16(20).ok().expect("something went horribly wrong");
                writer.write_be_u16(41).ok().expect("something went horribly wrong");

            },
            AccessRequest(ref f_realm, ref f_exclusive, ref f_passive, ref f_active, ref f_write, ref f_read) => {
                writer.write_be_u16(30).ok().expect("something went horribly wrong");
                writer.write_be_u16(10).ok().expect("something went horribly wrong");

                f_realm.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exclusive.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_passive.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_active.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_write.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_read.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            AccessRequestOk(ref f_ticket) => {
                writer.write_be_u16(30).ok().expect("something went horribly wrong");
                writer.write_be_u16(11).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ExchangeDeclare(ref f_ticket, ref f_exchange, ref f_type, ref f_passive, ref f_durable, ref f_auto_delete, ref f_internal, ref f_nowait, ref f_arguments) => {
                writer.write_be_u16(40).ok().expect("something went horribly wrong");
                writer.write_be_u16(10).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exchange.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_type.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_passive.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_durable.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_auto_delete.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_internal.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_arguments.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ExchangeDeclareOk => {
                writer.write_be_u16(40).ok().expect("something went horribly wrong");
                writer.write_be_u16(11).ok().expect("something went horribly wrong");

            },
            ExchangeDelete(ref f_ticket, ref f_exchange, ref f_if_unused, ref f_nowait) => {
                writer.write_be_u16(40).ok().expect("something went horribly wrong");
                writer.write_be_u16(20).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exchange.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_if_unused.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ExchangeDeleteOk => {
                writer.write_be_u16(40).ok().expect("something went horribly wrong");
                writer.write_be_u16(21).ok().expect("something went horribly wrong");

            },
            ExchangeBind(ref f_ticket, ref f_destination, ref f_source, ref f_routing_key, ref f_nowait, ref f_arguments) => {
                writer.write_be_u16(40).ok().expect("something went horribly wrong");
                writer.write_be_u16(30).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_destination.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_source.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_routing_key.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_arguments.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ExchangeBindOk => {
                writer.write_be_u16(40).ok().expect("something went horribly wrong");
                writer.write_be_u16(31).ok().expect("something went horribly wrong");

            },
            ExchangeUnbind(ref f_ticket, ref f_destination, ref f_source, ref f_routing_key, ref f_nowait, ref f_arguments) => {
                writer.write_be_u16(40).ok().expect("something went horribly wrong");
                writer.write_be_u16(40).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_destination.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_source.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_routing_key.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_arguments.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ExchangeUnbindOk => {
                writer.write_be_u16(40).ok().expect("something went horribly wrong");
                writer.write_be_u16(51).ok().expect("something went horribly wrong");

            },
            QueueDeclare(ref f_ticket, ref f_queue, ref f_passive, ref f_durable, ref f_exclusive, ref f_auto_delete, ref f_nowait, ref f_arguments) => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(10).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_queue.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_passive.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_durable.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exclusive.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_auto_delete.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_arguments.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            QueueDeclareOk(ref f_queue, ref f_message_count, ref f_consumer_count) => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(11).ok().expect("something went horribly wrong");

                f_queue.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_message_count.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_consumer_count.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            QueueBind(ref f_ticket, ref f_queue, ref f_exchange, ref f_routing_key, ref f_nowait, ref f_arguments) => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(20).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_queue.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exchange.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_routing_key.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_arguments.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            QueueBindOk => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(21).ok().expect("something went horribly wrong");

            },
            QueuePurge(ref f_ticket, ref f_queue, ref f_nowait) => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(30).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_queue.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            QueuePurgeOk(ref f_message_count) => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(31).ok().expect("something went horribly wrong");

                f_message_count.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            QueueDelete(ref f_ticket, ref f_queue, ref f_if_unused, ref f_if_empty, ref f_nowait) => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(40).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_queue.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_if_unused.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_if_empty.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            QueueDeleteOk(ref f_message_count) => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(41).ok().expect("something went horribly wrong");

                f_message_count.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            QueueUnbind(ref f_ticket, ref f_queue, ref f_exchange, ref f_routing_key, ref f_arguments) => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(50).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_queue.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exchange.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_routing_key.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_arguments.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            QueueUnbindOk => {
                writer.write_be_u16(50).ok().expect("something went horribly wrong");
                writer.write_be_u16(51).ok().expect("something went horribly wrong");

            },
            BasicQos(ref f_prefetch_size, ref f_prefetch_count, ref f_global) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(10).ok().expect("something went horribly wrong");

                f_prefetch_size.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_prefetch_count.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_global.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicQosOk => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(11).ok().expect("something went horribly wrong");

            },
            BasicConsume(ref f_ticket, ref f_queue, ref f_consumer_tag, ref f_no_local, ref f_no_ack, ref f_exclusive, ref f_nowait, ref f_arguments) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(20).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_queue.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_consumer_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_no_local.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_no_ack.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exclusive.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_arguments.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicConsumeOk(ref f_consumer_tag) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(21).ok().expect("something went horribly wrong");

                f_consumer_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicCancel(ref f_consumer_tag, ref f_nowait) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(30).ok().expect("something went horribly wrong");

                f_consumer_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicCancelOk(ref f_consumer_tag) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(31).ok().expect("something went horribly wrong");

                f_consumer_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicPublish(ref f_ticket, ref f_exchange, ref f_routing_key, ref f_mandatory, ref f_immediate) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(40).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exchange.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_routing_key.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_mandatory.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_immediate.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicReturn(ref f_reply_code, ref f_reply_text, ref f_exchange, ref f_routing_key) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(50).ok().expect("something went horribly wrong");

                f_reply_code.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_reply_text.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exchange.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_routing_key.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicDeliver(ref f_consumer_tag, ref f_delivery_tag, ref f_redelivered, ref f_exchange, ref f_routing_key) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(60).ok().expect("something went horribly wrong");

                f_consumer_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_delivery_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_redelivered.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exchange.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_routing_key.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicGet(ref f_ticket, ref f_queue, ref f_no_ack) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(70).ok().expect("something went horribly wrong");

                f_ticket.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_queue.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_no_ack.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicGetOk(ref f_delivery_tag, ref f_redelivered, ref f_exchange, ref f_routing_key, ref f_message_count) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(71).ok().expect("something went horribly wrong");

                f_delivery_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_redelivered.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_exchange.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_routing_key.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_message_count.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicGetEmpty(ref f_cluster_id) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(72).ok().expect("something went horribly wrong");

                f_cluster_id.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicAck(ref f_delivery_tag, ref f_multiple) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(80).ok().expect("something went horribly wrong");

                f_delivery_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_multiple.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicReject(ref f_delivery_tag, ref f_requeue) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(90).ok().expect("something went horribly wrong");

                f_delivery_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_requeue.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicRecoverAsync(ref f_requeue) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(100).ok().expect("something went horribly wrong");

                f_requeue.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicRecover(ref f_requeue) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(110).ok().expect("something went horribly wrong");

                f_requeue.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            BasicRecoverOk => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(111).ok().expect("something went horribly wrong");

            },
            BasicNack(ref f_delivery_tag, ref f_multiple, ref f_requeue) => {
                writer.write_be_u16(60).ok().expect("something went horribly wrong");
                writer.write_be_u16(120).ok().expect("something went horribly wrong");

                f_delivery_tag.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_multiple.to_writer(&mut writer).ok().expect("something went horribly wrong");
                f_requeue.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            TxSelect => {
                writer.write_be_u16(90).ok().expect("something went horribly wrong");
                writer.write_be_u16(10).ok().expect("something went horribly wrong");

            },
            TxSelectOk => {
                writer.write_be_u16(90).ok().expect("something went horribly wrong");
                writer.write_be_u16(11).ok().expect("something went horribly wrong");

            },
            TxCommit => {
                writer.write_be_u16(90).ok().expect("something went horribly wrong");
                writer.write_be_u16(20).ok().expect("something went horribly wrong");

            },
            TxCommitOk => {
                writer.write_be_u16(90).ok().expect("something went horribly wrong");
                writer.write_be_u16(21).ok().expect("something went horribly wrong");

            },
            TxRollback => {
                writer.write_be_u16(90).ok().expect("something went horribly wrong");
                writer.write_be_u16(30).ok().expect("something went horribly wrong");

            },
            TxRollbackOk => {
                writer.write_be_u16(90).ok().expect("something went horribly wrong");
                writer.write_be_u16(31).ok().expect("something went horribly wrong");

            },
            ConfirmSelect(ref f_nowait) => {
                writer.write_be_u16(85).ok().expect("something went horribly wrong");
                writer.write_be_u16(10).ok().expect("something went horribly wrong");

                f_nowait.to_writer(&mut writer).ok().expect("something went horribly wrong");
            },
            ConfirmSelectOk => {
                writer.write_be_u16(85).ok().expect("something went horribly wrong");
                writer.write_be_u16(11).ok().expect("something went horribly wrong");

            },
        }

        writer.unwrap()
    }

    pub fn to_frame(&self, channel: Channel) -> Frame {
        MethodFrame(channel, self.payload())
    }
}
