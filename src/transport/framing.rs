use std::io;

use transport::types::*;
use transport::serialization::{Readable,Writable};

pub type Channel = u16;
pub type ClassId = u16;
pub type MethodId = u16;
pub type ContentHeaderPropertyFlags = Vec<bool>;
pub type ContentHeaderProperties = Vec<Field>;

#[deriving(Show)]
pub enum Frame {
	MethodFrame(Channel, Vec<u8>),
	ContentHeaderFrame(Channel, ClassId, u64, ContentHeaderPropertyFlags),
	ContentBodyFrame(Channel, Vec<u8>),
	HeartbeatFrame,
}

pub struct FrameBuffer<R> {
	reader: R,
}

impl Frame {
	pub fn encode(&self) -> Vec<u8> {
		unimplemented!()
	}
}

impl Writable for Frame {
	fn to_writer(&self, writer: &mut Writer) -> io::IoResult<()> {
		match *self {
			MethodFrame(channel, ref payload) => {
				try!(writer.write_u8(1));
				try!(writer.write_be_u16(channel));
				try!(writer.write_be_u32(payload.len() as u32));
				try!(writer.write(payload.as_slice()));
				try!(writer.write_u8(0xCE));
			},
			_ => unimplemented!(),
		}

		Ok(())
	}
}

impl Readable for Frame {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Frame> {
		let frame_type = try!(reader.read_u8());

		let channel = try!(reader.read_be_u16());

		let size = try!(reader.read_be_u32());

		let payload = try!(reader.read_exact(size as uint));

		let frame_end = try!(reader.read_u8());
		if frame_end != 0xCE {
			fail!("invalid frame end!");
		}

		let frame = match frame_type {
			1 => MethodFrame(channel, payload),
			2 => ContentHeaderFrame(channel, 0, 0, vec!()),
			3 => ContentBodyFrame(channel, vec!()),
			8 => HeartbeatFrame,
			_ => unreachable!()
		};

		Ok(frame)
	}
}

impl<R: io::Reader> FrameBuffer<R> {
	pub fn new(reader: R) -> FrameBuffer<R> {
		FrameBuffer {
			reader: reader,
		}
	}

	pub fn read_frame(&mut self) -> io::IoResult<Frame> {
		Readable::from_reader(&mut self.reader)
	}

	pub fn unwrap(self) -> R {
		self.reader
	}
}
