use std::io::Timer;
use time;

use transport::framing;
use transport::serialization::Writable;

pub struct HeartbeatSender<T> {
    writer: T,
    timer: Timer,
    interval: u64,
    sender: Option<Sender<time::Tm>>,
}

impl<T: Writer> HeartbeatSender<T> {
    /// Creates a new heartbeat sender which sends a heartbeat ever `interval` milliseconds to the
    /// `writer`.
    pub fn new(writer: T) -> HeartbeatSender<T> {
        HeartbeatSender {
            writer: writer,
            timer: Timer::new().unwrap(),
            interval: 0,
            sender: None,
        }
    }

    pub fn set_interval(&mut self, interval: u64) {
        self.interval = interval;
    }

    /// Start the heartbeat sender.
    pub fn start(&mut self) {
        let (tx, rx) = channel();
        self.sender = Some(tx);
        let interval = self.interval;
        let mut writer = self.writer.clone();

        spawn(proc() {
            let mut timer = Timer::new().unwrap();
            loop {
                let timeout = timer.oneshot(interval);

                select! {
                    _ = rx.recv() => {}, // Got data, let's loop around.
                    _ = timeout.recv() => {
                        // Send heartbeat
                        let frame = framing::HeartbeatFrame;
                        match frame.to_writer(&mut writer) {
                            Ok(_) => {},
                            Err(_) => {
                                drop(rx);
                                break;
                            }
                        }
                    }
                }
            }
        })
    }

    pub fn signal_activity(&mut self) -> Option<()> {
        match self.sender {
            Some(ref sender) => sender.send_opt(time::now_utc()).ok(),
            None => None,
        }
    }
}
