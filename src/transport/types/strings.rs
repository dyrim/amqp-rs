use std::fmt;
use std::mem;
use std::str;

/// Short strings are up to 255 bytes of UTF-8 data. It may not contain any binary zero octets.
#[deriving(PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub struct ShortString(String);

/// Long strings are binary data up to 2^32 in length.
pub struct LongString(Vec<u8>);

pub trait ShortStringCast {
    /// Convert to a ShortString.
    ///
    /// Fails if string is not a valid ShortString (contains null bytes, i s too long or is not
    /// valid UTF-8 data).
    fn to_short_string(&self) -> ShortString {
        assert!(self.valid_short_string());
        unsafe { self.to_short_string_nocheck() }
    }

    /// Convert to a ShortString.
    ///
    /// Returns None if the string is not a valid ShortString.
    fn to_short_string_opt(&self) -> Option<ShortString> {
        if self.valid_short_string() {
            Some(unsafe { self.to_short_string_nocheck() })
        } else {
            None
        }
    }

    /// Convert to a ShortString without doing any value asserts.
    unsafe fn to_short_string_nocheck(&self) -> ShortString;

    /// Check if this can be represented as a valid ShortString.
    ///
    /// Valid short strings:
    ///   - Are never longer than 255 bytes.
    ///   - Are valid UTF-8.
    ///   - Do not contain null bytes.
    fn valid_short_string(&self) -> bool;
}

impl ShortStringCast for ShortString {
    unsafe fn to_short_string_nocheck(&self) -> ShortString {
        self.clone()
    }

    fn valid_short_string(&self) -> bool {
        true
    }
}

impl<'a> ShortStringCast for &'a str {
    unsafe fn to_short_string_nocheck(&self) -> ShortString {
        mem::transmute(String::from_str(*self))
    }

    fn valid_short_string(&self) -> bool {
        self.len() <= 255 && self.bytes().all(|b| b != 0)
    }
}

impl ShortStringCast for String {
    unsafe fn to_short_string_nocheck(&self) -> ShortString {
        mem::transmute(self.clone())
    }

    fn valid_short_string(&self) -> bool {
        let bytes = self.as_bytes();
        bytes.len() <= 255 && bytes.iter().all(|b| *b != 0)
    }
}

impl Str for ShortString {
    fn as_slice<'a>(&'a self) -> &'a str {
        self.data()
    }
}

impl fmt::Show for ShortString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pad(self.as_slice())
    }
}

impl Collection for ShortString {
    fn len(&self) -> uint {
        self.data().len()
    }

    fn is_empty(&self) -> bool {
        self.data().is_empty()
    }
}

impl ShortString {
    pub fn new(data: String) -> ShortString {
        ShortString(data)
    }

    pub fn from<T: ShortStringCast>(data: T) -> Result<ShortString, T> {
        match data.to_short_string_opt() {
            Some(short_string) => Ok(short_string),
            None => Err(data)
        }
    }

    pub fn as_bytes<'a>(&'a self) -> &'a [u8] {
        self.data().as_bytes()
    }

    fn data<'a>(&'a self) -> &'a str {
        let ShortString(ref data) = *self;
        data.as_slice()
    }
}


impl Collection for LongString {
    fn len(&self) -> uint {
        self.data().len()
    }

    fn is_empty(&self) -> bool {
        self.data().is_empty()
    }
}

impl fmt::Show for LongString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match str::from_utf8(self.as_bytes()) {
            Some(s) => f.pad(s),
            None => self.as_bytes().fmt(f)
        }
    }
}

impl LongString {
    pub fn from_str<'a>(data: &'a str) -> LongString {
        LongString::new(data.as_bytes().to_owned())
    }

    pub fn new(data: Vec<u8>) -> LongString {
        LongString(data)
    }

    pub fn as_bytes<'a>(&'a self) -> &'a [u8] {
        self.data()
    }

    fn data<'a>(&'a self) -> &'a [u8] {
        let LongString(ref data) = *self;
        data.as_slice()
    }
}

#[cfg(test)]
mod test {
    use super::{ShortString,LongString,ShortStringCast};

    #[test]
    fn test_borrowed_str_to_short_string() {
        let valid = "hello world";
        let toolong_string = "a".repeat(256);
        let toolong = toolong_string.as_slice();
        let nullbyte = "hello\x00 world";

        assert_eq!(valid.to_short_string_opt(), Some(ShortString::new("hello world".to_string())));
        assert_eq!(toolong.to_short_string_opt(), None);
        assert_eq!(nullbyte.to_short_string_opt(), None);
    }
}
