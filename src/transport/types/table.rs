use std::collections::HashMap;
use std::collections::hashmap;

use super::Boolean;
use super::ShortShortInt;
use super::ShortShortUint;
use super::ShortInt;
use super::ShortUint;
use super::LongInt;
use super::LongUint;
use super::LongLongInt;
use super::LongLongUint;
use super::Float;
use super::Double;
use super::Decimal;
use super::ShortString;
use super::ShortStringCast;
use super::LongString;
use super::Array;
use super::Timestamp;

#[deriving(Show)]
pub enum TableValue {
	BooleanValue(Boolean),
	ShortShortIntValue(ShortShortInt),
	ShortShortUintValue(ShortShortUint),
	ShortIntValue(ShortInt),
	ShortUintValue(ShortUint),
	LongIntValue(LongInt),
	LongUintValue(LongUint),
	LongLongIntValue(LongLongInt),
	LongLongUintValue(LongLongUint),
	FloatValue(Float),
	DoubleValue(Double),
	DecimalValue(Decimal),
	ShortStringValue(ShortString),
	LongStringValue(LongString),
	ArrayValue(Array),
	TimestampValue(Timestamp),
	TableValue(Table),
	NoValue,
}

#[deriving(Show)]
pub struct Table {
	pairs: HashMap<ShortString,TableValue>,
}

pub trait TableValueCast {
	fn into_table_value(self) -> Option<TableValue>;
}

impl TableValueCast for bool {
	fn into_table_value(self) -> Option<TableValue> {
		Some(BooleanValue(self))
	}
}

impl TableValueCast for Table {
	fn into_table_value(self) -> Option<TableValue> {
		Some(TableValue(self))
	}
}

impl<'a> TableValueCast for &'a str {
	fn into_table_value(self) -> Option<TableValue> {
        self.to_short_string_opt().map(|s| ShortStringValue(s))
	}
}

impl TableValueCast for TableValue {
	fn into_table_value(self) -> Option<TableValue> {
		Some(self)
	}
}

impl Collection for Table {
	fn len(&self) -> uint {
		self.pairs.len()
	}

	fn is_empty(&self) -> bool {
		self.pairs.is_empty()
	}
}

impl Map<ShortString, TableValue> for Table {
	fn find<'a>(&'a self, key: &ShortString) -> Option<&'a TableValue> {
		self.pairs.find(key)
	}
}

impl Table {
	pub fn new() -> Table {
		Table { pairs: HashMap::new() }
	}

	pub fn find<'a>(&'a self, key: &str) -> Option<&'a TableValue> {
		match ShortString::from(key) {
			Ok(key) => self.pairs.find(&key),
			Err(_) => None,
		}
	}

	pub fn iter<'a>(&'a self) -> hashmap::Entries<'a, ShortString, TableValue> {
		self.pairs.iter()
	}

	pub fn insert<K: ShortStringCast, V: TableValueCast>(&mut self, name: K, value: V) -> bool {
		match (name.to_short_string_opt(), value.into_table_value()) {
			(Some(key), Some(value)) => self.pairs.insert(key, value),
			_ => false
		}
	}
}
