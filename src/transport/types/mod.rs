use time;

pub use self::table::Table;
pub use self::table::TableValue;
pub use self::table::{BooleanValue,ShortShortIntValue,ShortShortUintValue,ShortIntValue,ShortUintValue,LongIntValue,LongUintValue,LongLongIntValue,LongLongUintValue,FloatValue,DoubleValue,DecimalValue,ShortStringValue,LongStringValue,ArrayValue,TimestampValue,TableValue,NoValue};
pub use self::strings::{ShortString,ShortStringCast};
pub use self::strings::LongString;

mod strings;
mod table;

#[deriving(Show)]
pub enum Field {
	BitField(u8),
	OctetField(u8),
	ShortUintField(ShortUint),
	LongUintField(LongUint),
	LongLongUintField(LongLongUint),
	ShortStringField(ShortString),
	LongStringField(LongString),
	TimestampField(Timestamp),
	TableField(Table),
}

pub type ShortShortUint = u8;
pub type ShortUint = u16;
pub type LongUint = u32;
pub type LongLongUint = u64;

pub type ShortShortInt = i8;
pub type ShortInt = i16;
pub type LongInt = i32;
pub type LongLongInt = i64;

pub type Boolean = bool;

pub type Float = f32;
pub type Double = f64;

#[deriving(Show)]
pub struct Decimal {
	pub scale: u8,
	pub value: u32,
}

pub type Array = Vec<TableValue>;


#[deriving(Show)]
pub struct Timestamp(i64);

impl Timestamp {
	pub fn new(sec: i64) -> Timestamp {
        Timestamp(sec)
	}

	pub fn to_timespec(&self) -> time::Timespec {
        let Timestamp(data) = *self;
		time::Timespec::new(data, 0)
	}
}
