use std::io;

use transport::types::*;

pub trait Writable {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()>;
}

pub trait Readable {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Self>;
}

impl Writable for TableValue {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		match *self {
			BooleanValue(boolean) => {
				try!(writer.write_u8(b't'));
				boolean.to_writer(writer)
			}
			ShortShortIntValue(short_short_int) => {
				try!(writer.write_u8(b'b'));
				short_short_int.to_writer(writer)
			},
			ShortShortUintValue(short_short_uint) => {
				try!(writer.write_u8(b'B'));
				short_short_uint.to_writer(writer)
			},
			ShortIntValue(short_int) => {
				try!(writer.write_u8(b'U'));
				short_int.to_writer(writer)
			},
			ShortUintValue(short_uint) => {
				try!(writer.write_u8(b'u'));
				short_uint.to_writer(writer)
			},
			LongIntValue(long_int) => {
				try!(writer.write_u8(b'I'));
				long_int.to_writer(writer)
			},
			LongUintValue(long_uint) => {
				try!(writer.write_u8(b'i'));
				long_uint.to_writer(writer)
			},
			LongLongIntValue(long_long_int) => {
				try!(writer.write_u8(b'L'));
				long_long_int.to_writer(writer)
			},
			LongLongUintValue(long_long_uint) => {
				try!(writer.write_u8(b'l'));
				long_long_uint.to_writer(writer)
			},
			FloatValue(float) => {
				try!(writer.write_u8(b'f'));
				float.to_writer(writer)
			},
			DoubleValue(double) => {
				try!(writer.write_u8(b'd'));
				double.to_writer(writer)
			},
			DecimalValue(decimal) => {
				try!(writer.write_u8(b'D'));
				decimal.to_writer(writer)
			},
			ShortStringValue(ref short_string) => {
				try!(writer.write_u8(b's'));
				short_string.to_writer(writer)
			},
			LongStringValue(ref long_string) => {
				try!(writer.write_u8(b'S'));
				long_string.to_writer(writer)
			},
			ArrayValue(ref array) => {
				try!(writer.write_u8(b'A'));
				array.to_writer(writer)
			},
			TimestampValue(timestamp) => {
				try!(writer.write_u8(b'T'));
				timestamp.to_writer(writer)
			},
			TableValue(ref table) => {
				try!(writer.write_u8(b'F'));
				table.to_writer(writer)
			},
			NoValue => {
				writer.write_u8(b'V')
			},
		}
	}
}

impl Readable for TableValue {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<TableValue> {
		match try!(reader.read_u8()) {
			b't' => Readable::from_reader(reader).map(|x| BooleanValue(x)),
			b'b' => Readable::from_reader(reader).map(|x| ShortShortIntValue(x)),
			b'B' => Readable::from_reader(reader).map(|x| ShortShortUintValue(x)),
			b'U' => Readable::from_reader(reader).map(|x| ShortIntValue(x)),
			b'u' => Readable::from_reader(reader).map(|x| ShortUintValue(x)),
			b'I' => Readable::from_reader(reader).map(|x| LongIntValue(x)),
			b'i' => Readable::from_reader(reader).map(|x| LongUintValue(x)),
			b'L' => Readable::from_reader(reader).map(|x| LongLongIntValue(x)),
			b'l' => Readable::from_reader(reader).map(|x| LongLongUintValue(x)),
			b'f' => Readable::from_reader(reader).map(|x| FloatValue(x)),
			b'd' => Readable::from_reader(reader).map(|x| DoubleValue(x)),
			b'D' => Readable::from_reader(reader).map(|x| DecimalValue(x)),
			b's' => Readable::from_reader(reader).map(|x| ShortStringValue(x)),
			b'S' => Readable::from_reader(reader).map(|x| LongStringValue(x)),
			b'A' => Readable::from_reader(reader).map(|x| ArrayValue(x)),
			b'T' => Readable::from_reader(reader).map(|x| TimestampValue(x)),
			b'F' => Readable::from_reader(reader).map(|x| TableValue(x)),
			b'V' => Ok(NoValue),
			_ => unreachable!()
		}
	}
}

impl Writable for Table {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		let mut w = io::MemWriter::new();

		for (name, value) in self.iter() {
			try!(name.to_writer(&mut w));
			try!(value.to_writer(&mut w));
		}

		try!(writer.write_be_u32(w.get_ref().len() as u32));
		writer.write(w.get_ref())
	}
}

impl Readable for Table {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Table> {
		let length = try!(reader.read_be_u32());
		let mut r = io::MemReader::new(try!(reader.read_exact(length as uint)));

		let mut table = Table::new();

		while !r.eof() {
			let name: ShortString = try!(Readable::from_reader(&mut r));
			let value: TableValue = try!(Readable::from_reader(&mut r));
			table.insert(name, value);
		}

		Ok(table)
	}
}

impl Writable for Field {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		match *self {
			BitField(_) => unimplemented!(),
			OctetField(octet) => writer.write_u8(octet),
			ShortUintField(short_uint) => short_uint.to_writer(writer),
			LongUintField(long_uint) => long_uint.to_writer(writer),
			LongLongUintField(long_long_uint) => long_long_uint.to_writer(writer),
			ShortStringField(ref short_string) => short_string.to_writer(writer),
			LongStringField(ref long_string) => long_string.to_writer(writer),
			TimestampField(timestamp) => timestamp.to_writer(writer),
			TableField(ref table) => table.to_writer(writer),
		}
	}
}

impl Writable for ShortShortUint {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_u8(*self)
	}
}

impl Readable for ShortShortUint {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<ShortShortUint> {
		reader.read_u8()
	}
}

impl Writable for ShortUint {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_u16(*self)
	}
}

impl Readable for ShortUint {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<ShortUint> {
		reader.read_be_u16()
	}
}

impl Writable for LongUint {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_u32(*self)
	}
}

impl Readable for LongUint {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<LongUint> {
		reader.read_be_u32()
	}
}

impl Writable for LongLongUint {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_u64(*self)
	}
}

impl Readable for LongLongUint {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<LongLongUint> {
		reader.read_be_u64()
	}
}

impl Writable for ShortShortInt {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_i8(*self)
	}
}

impl Readable for ShortShortInt {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<ShortShortInt> {
		reader.read_i8()
	}
}

impl Writable for ShortInt {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_i16(*self)
	}
}

impl Readable for ShortInt {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<ShortInt> {
		reader.read_be_i16()
	}
}

impl Writable for LongInt {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_i32(*self)
	}
}

impl Readable for LongInt {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<LongInt> {
		reader.read_be_i32()
	}
}

impl Writable for LongLongInt {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_i64(*self)
	}
}

impl Readable for LongLongInt {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<LongLongInt> {
		reader.read_be_i64()
	}
}

impl Writable for Boolean {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		match *self {
			true => writer.write_u8(1),
			false => writer.write_u8(0),
		}
	}
}

impl Readable for Boolean {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Boolean> {
		reader.read_u8().map(|b| b != 0)
	}
}

impl Writable for Float {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_f32(*self)
	}
}

impl Readable for Float {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Float> {
		reader.read_be_f32()
	}
}

impl Writable for Double {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_f64(*self)
	}
}

impl Readable for Double {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Double> {
		reader.read_be_f64()
	}
}

impl Writable for Decimal {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		try!(writer.write_u8(self.scale));
		writer.write_be_u32(self.value)
	}
}

impl Readable for Decimal {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Decimal> {
		let scale = try!(reader.read_u8());
		let value = try!(reader.read_be_u32());

		Ok(Decimal { scale: scale, value: value })
	}
}

impl Writable for Array {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		let mut w = io::MemWriter::new();
		for field in self.iter() {
			try!(field.to_writer(&mut w));
		}

		try!(writer.write_be_u32(w.get_ref().len() as u32));
		writer.write(w.get_ref())
	}
}

impl Readable for Array {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Array> {
		let length = try!(reader.read_be_u32());
		let mut r = io::MemReader::new(try!(reader.read_exact(length as uint)));

		let mut items = Vec::new();

		while !r.eof() {
			items.push(try!(Readable::from_reader(&mut r)));
		}

		Ok(items)
	}
}

impl Writable for ShortString {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		try!(writer.write_u8(self.len() as u8));
		writer.write(self.as_bytes())
	}
}

impl Readable for ShortString {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<ShortString> {
		let length = try!(reader.read_u8());
		let mut r = io::MemReader::new(try!(reader.read_exact(length as uint)));

		r.read_to_string().map(|data| ShortString::new(data))
	}
}

impl Writable for LongString {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		try!(writer.write_be_u32(self.len() as u32));
		writer.write(self.as_bytes())
	}
}

impl Readable for LongString {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<LongString> {
		let length = try!(reader.read_be_u32());

		reader.read_exact(length as uint).map(|data| LongString::new(data))
	}
}

impl Writable for Timestamp {
	fn to_writer(&self, writer: &mut io::Writer) -> io::IoResult<()> {
		writer.write_be_i64(self.to_timespec().sec)
	}
}

impl Readable for Timestamp {
	fn from_reader(reader: &mut io::Reader) -> io::IoResult<Timestamp> {
		reader.read_be_i64().map(|data| Timestamp::new(data))
	}
}
