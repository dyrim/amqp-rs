#![cfg(test)]
mod test {
	use frame::{Method};
	use protocol::{channel};

	#[test]
	fn test_channel_open() {
		let frame = channel::Open::new().encode(1);
		match frame {
			Method(frame) => {
				assert_eq!(frame.channel, 1);
				assert_eq!(frame.payload, vec![0x00, 0x14, 0x00, 0x0A, 0x00]);
			},
			_ => fail!("expected method frame")
		}
	}
}
