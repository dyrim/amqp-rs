// Generated code. Do not edit. Edit and re-run codegen.py instead.

static FRAME_METHOD: uint = 1;
static FRAME_HEADER: uint = 2;
static FRAME_BODY: uint = 3;
static FRAME_HEARTBEAT: uint = 8;
static FRAME_MIN_SIZE: uint = 4096;
static FRAME_END: uint = 206;
static REPLY_SUCCESS: uint = 200;
static CONTENT_TOO_LARGE: uint = 311;
static NO_ROUTE: uint = 312;
static NO_CONSUMERS: uint = 313;
static ACCESS_REFUSED: uint = 403;
static NOT_FOUND: uint = 404;
static RESOURCE_LOCKED: uint = 405;
static PRECONDITION_FAILED: uint = 406;
static CONNECTION_FORCED: uint = 320;
static INVALID_PATH: uint = 402;
static FRAME_ERROR: uint = 501;
static SYNTAX_ERROR: uint = 502;
static COMMAND_INVALID: uint = 503;
static CHANNEL_ERROR: uint = 504;
static UNEXPECTED_FRAME: uint = 505;
static RESOURCE_ERROR: uint = 506;
static NOT_ALLOWED: uint = 530;
static NOT_IMPLEMENTED: uint = 540;
static INTERNAL_ERROR: uint = 541;

pub mod connection {
    pub struct Start;

    impl Start {
        pub fn new() -> Start {
            Start
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct StartOk;

    impl StartOk {
        pub fn new() -> StartOk {
            StartOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Secure;

    impl Secure {
        pub fn new() -> Secure {
            Secure
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct SecureOk;

    impl SecureOk {
        pub fn new() -> SecureOk {
            SecureOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Tune;

    impl Tune {
        pub fn new() -> Tune {
            Tune
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct TuneOk;

    impl TuneOk {
        pub fn new() -> TuneOk {
            TuneOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Open;

    impl Open {
        pub fn new() -> Open {
            Open
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct OpenOk;

    impl OpenOk {
        pub fn new() -> OpenOk {
            OpenOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Close;

    impl Close {
        pub fn new() -> Close {
            Close
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct CloseOk;

    impl CloseOk {
        pub fn new() -> CloseOk {
            CloseOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Blocked;

    impl Blocked {
        pub fn new() -> Blocked {
            Blocked
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Unblocked;

    impl Unblocked {
        pub fn new() -> Unblocked {
            Unblocked
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

}

pub mod channel {
    pub struct Open;

    impl Open {
        pub fn new() -> Open {
            Open
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct OpenOk;

    impl OpenOk {
        pub fn new() -> OpenOk {
            OpenOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Flow;

    impl Flow {
        pub fn new() -> Flow {
            Flow
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct FlowOk;

    impl FlowOk {
        pub fn new() -> FlowOk {
            FlowOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Close;

    impl Close {
        pub fn new() -> Close {
            Close
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct CloseOk;

    impl CloseOk {
        pub fn new() -> CloseOk {
            CloseOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

}

pub mod access {
    pub struct Request;

    impl Request {
        pub fn new() -> Request {
            Request
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct RequestOk;

    impl RequestOk {
        pub fn new() -> RequestOk {
            RequestOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

}

pub mod exchange {
    pub struct Declare;

    impl Declare {
        pub fn new() -> Declare {
            Declare
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct DeclareOk;

    impl DeclareOk {
        pub fn new() -> DeclareOk {
            DeclareOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Delete;

    impl Delete {
        pub fn new() -> Delete {
            Delete
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct DeleteOk;

    impl DeleteOk {
        pub fn new() -> DeleteOk {
            DeleteOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Bind;

    impl Bind {
        pub fn new() -> Bind {
            Bind
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct BindOk;

    impl BindOk {
        pub fn new() -> BindOk {
            BindOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Unbind;

    impl Unbind {
        pub fn new() -> Unbind {
            Unbind
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct UnbindOk;

    impl UnbindOk {
        pub fn new() -> UnbindOk {
            UnbindOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

}

pub mod queue {
    pub struct Declare;

    impl Declare {
        pub fn new() -> Declare {
            Declare
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct DeclareOk;

    impl DeclareOk {
        pub fn new() -> DeclareOk {
            DeclareOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Bind;

    impl Bind {
        pub fn new() -> Bind {
            Bind
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct BindOk;

    impl BindOk {
        pub fn new() -> BindOk {
            BindOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Purge;

    impl Purge {
        pub fn new() -> Purge {
            Purge
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct PurgeOk;

    impl PurgeOk {
        pub fn new() -> PurgeOk {
            PurgeOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Delete;

    impl Delete {
        pub fn new() -> Delete {
            Delete
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct DeleteOk;

    impl DeleteOk {
        pub fn new() -> DeleteOk {
            DeleteOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Unbind;

    impl Unbind {
        pub fn new() -> Unbind {
            Unbind
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct UnbindOk;

    impl UnbindOk {
        pub fn new() -> UnbindOk {
            UnbindOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

}

pub mod basic {
    pub struct Qos;

    impl Qos {
        pub fn new() -> Qos {
            Qos
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct QosOk;

    impl QosOk {
        pub fn new() -> QosOk {
            QosOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Consume;

    impl Consume {
        pub fn new() -> Consume {
            Consume
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct ConsumeOk;

    impl ConsumeOk {
        pub fn new() -> ConsumeOk {
            ConsumeOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Cancel;

    impl Cancel {
        pub fn new() -> Cancel {
            Cancel
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct CancelOk;

    impl CancelOk {
        pub fn new() -> CancelOk {
            CancelOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Publish;

    impl Publish {
        pub fn new() -> Publish {
            Publish
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Return;

    impl Return {
        pub fn new() -> Return {
            Return
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Deliver;

    impl Deliver {
        pub fn new() -> Deliver {
            Deliver
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Get;

    impl Get {
        pub fn new() -> Get {
            Get
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct GetOk;

    impl GetOk {
        pub fn new() -> GetOk {
            GetOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct GetEmpty;

    impl GetEmpty {
        pub fn new() -> GetEmpty {
            GetEmpty
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Ack;

    impl Ack {
        pub fn new() -> Ack {
            Ack
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Reject;

    impl Reject {
        pub fn new() -> Reject {
            Reject
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct RecoverAsync;

    impl RecoverAsync {
        pub fn new() -> RecoverAsync {
            RecoverAsync
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Recover;

    impl Recover {
        pub fn new() -> Recover {
            Recover
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct RecoverOk;

    impl RecoverOk {
        pub fn new() -> RecoverOk {
            RecoverOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Nack;

    impl Nack {
        pub fn new() -> Nack {
            Nack
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

}

pub mod tx {
    pub struct Select;

    impl Select {
        pub fn new() -> Select {
            Select
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct SelectOk;

    impl SelectOk {
        pub fn new() -> SelectOk {
            SelectOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Commit;

    impl Commit {
        pub fn new() -> Commit {
            Commit
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct CommitOk;

    impl CommitOk {
        pub fn new() -> CommitOk {
            CommitOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct Rollback;

    impl Rollback {
        pub fn new() -> Rollback {
            Rollback
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct RollbackOk;

    impl RollbackOk {
        pub fn new() -> RollbackOk {
            RollbackOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

}

pub mod confirm {
    pub struct Select;

    impl Select {
        pub fn new() -> Select {
            Select
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

    pub struct SelectOk;

    impl SelectOk {
        pub fn new() -> SelectOk {
            SelectOk
        }

        pub fn encode(&self) -> Vec<u8> {
            vec![]
        }

    }

}

