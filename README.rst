=======
AMQP.rs
=======

Status: Work In Progress
========================

AMQP.rs is a work in progress. I'm using it in part to learn Rust, in part to get more familar with AMQP. I've already started from scratch a few times, but I think I'm getting closer to the approach I want to take.

I am deliberately not versioning this yet, as it is (1) not really ready to do anything useful and (2) any of the APIs may change.

That said, if you want to get involved at all, or have any feedback, please do get in touch. Either open a ticket, ping me `on Twitter`_, or `email me`_.

License
=======

This library is distributed under the MIT License. See the LICENSE file for more information.

.. _on Twitter: https://twitter.com/_dyrim
.. _email me: dyrim@dyrim.org
